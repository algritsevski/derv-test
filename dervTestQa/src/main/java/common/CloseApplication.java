package common;


import org.junit.Before;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskComponent;


public class CloseApplication {
		
    private QaAutomationEngineerTestTaskComponent component = QaAutomationEngineerTestTaskComponent.getInstance();
    
    @Before
    public void setUp() throws Exception {
        component.getDriver();
    }
       
    @Test
    public void testCloseApplication() throws Exception {
    	component.tearDown();
    	}
}

