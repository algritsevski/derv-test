package common;


import org.junit.Test;
import org.apache.log4j.BasicConfigurator;

import dervtest.QaAutomationEngineerTestTaskComponent;
import dervtest.QaAutomationEngineerTestTaskFixture;


public class OpenApplication {
	
	private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();	
    private QaAutomationEngineerTestTaskComponent component = QaAutomationEngineerTestTaskComponent.getInstance();
             
    @Test
    public void testOpenApplication() throws Exception {
    	BasicConfigurator.configure();
    	component.getDriver();
    	fixture.openPage();  	
    	
    	}
}

