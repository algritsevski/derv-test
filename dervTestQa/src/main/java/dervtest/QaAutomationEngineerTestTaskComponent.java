package dervtest;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static org.junit.Assert.fail;

public class QaAutomationEngineerTestTaskComponent {

    private static final QaAutomationEngineerTestTaskComponent INSTANCE = new QaAutomationEngineerTestTaskComponent();
    
    private StringBuffer verificationErrors = new StringBuffer();

    public WebDriver driver;
    
    private static Logger logger = Logger.getLogger(QaAutomationEngineerTestTaskComponent.class);
    
    private static final int TIMEOUT_IN_MILLISEC = 10000;

    @SuppressWarnings("deprecation")
	private QaAutomationEngineerTestTaskComponent() {
      System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
      DesiredCapabilities handlSSLErr = DesiredCapabilities.chrome ();       
  	  handlSSLErr.setCapability (CapabilityType.ACCEPT_SSL_CERTS, true);
  	  driver = new ChromeDriver (handlSSLErr);
  	  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
  	  driver.manage().window().maximize();
    }

    public static QaAutomationEngineerTestTaskComponent getInstance() {
        return INSTANCE;
    }


    public void waitElement(By by, Integer limit) throws InterruptedException {
        for (int second = 0; ; second++) {
            if (second >= limit) fail("timeout");
            try {
                if (isElementPresent(by)) break;
            } catch (Exception e) {
            }
            Thread.sleep(1000);
        }
    }

       
    public WebDriver getDriver() {
        return driver;
    }
    
   
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
          fail(verificationErrorString);
        }
    }
    public WebElement findElementQuetly(By by) {
       
        	WebElement element = driver.findElement(by);
        	if (element.isEnabled() && element.isDisplayed()) 
        		return driver.findElement(by);
            else
                return null;
    }

    public boolean isElementPresent(By by) {
    	if (findElementQuetly(by) == null) return false;
        return true;
       }
    

    public boolean waitElementPresent(By by) {
        Wait<WebDriver> wait = new WebDriverWait(driver, TIMEOUT_IN_MILLISEC);
        return wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                WebElement element = driver.findElement(by);
                for (int i=1; i<TIMEOUT_IN_MILLISEC; i=i+1000)
                {
	                if (element.isEnabled() && element.isDisplayed()) 
	                	return true;
	                else
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}    
                }
                logger.error("The Element " + by + " is not appears in the certain amount of time.");
                return false;
            }
        });
    }

    public boolean waitForTextExist(By by, String text) {
        Wait<WebDriver> wait = new WebDriverWait(driver, TIMEOUT_IN_MILLISEC);
        return wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                WebElement element = driver.findElement(by);
                for (int i=1; i<TIMEOUT_IN_MILLISEC; i=i+1000)
                {
                	if (element.getText().equals(text)) 
                		return true;                		
					else
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
                }
                logger.error("The text: '" + text + "' is not appears in the certain amount of time.");
                return false;
            }
            
        });
    }
}
