package dervtest;


import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class QaAutomationEngineerTestTaskFixture {
	
	private static final QaAutomationEngineerTestTaskFixture INSTANCE = new QaAutomationEngineerTestTaskFixture();
	private static Logger logger = Logger.getLogger(QaAutomationEngineerTestTaskFixture.class);
	private String baseUrl;


    private QaAutomationEngineerTestTaskComponent component = QaAutomationEngineerTestTaskComponent.getInstance();
   

    public boolean openPage() throws InterruptedException {
		  

		   baseUrl = "file:///C:/workspace/bitbucket/Test_Task.html";
		   component.driver.get(baseUrl);
		  	
		    if (component.waitElementPresent(By.id("testdata"))) {
		    	}
			else
			  	{
		  		logger.error("The page is no aviable - further actions are abandoned");
			   		return false;
		    }
		   
		   if (component.waitElementPresent(By.id("balance-value"))) {
			   	   return true; 
			   }
			else
			  	{
			   		logger.error("The balance value is no aviable - further actions are abandoned");
			   		return false;
			    }
		   }
    
      
    
    public Integer getBalance() {
  	  
  	String balance = component.driver.findElement(By.id("balance-value")).getAttribute("value");
  	
  	   return Integer.parseInt(balance);
  	}
    
    public boolean runGameWithTestData(String testData) throws InterruptedException {
		
    	component.waitElementPresent(By.id("spinButton"));
    	component.driver.findElement(By.id("testdata")).clear();
	    component.driver.findElement(By.id("testdata")).sendKeys(testData);
	    component.driver.findElement(By.id("spinButton")).click();
    	
    	return true;
		  
    }
    
  public boolean checkWinInformation(String winAmount) throws InterruptedException {
	
	String actualWinInformation  = component.driver.findElement(By.id("winbox")).getText();
	  
	try {
   		assertEquals("Win " + winAmount + " coins", actualWinInformation);
   	} catch (Error e) {
   		logger.error("Information about win is incorrect. Expexted: Win " + winAmount + " coins. Actual: " + actualWinInformation );
   		return false;
   	}
    	
    	return true;
		  
    }
  
  public boolean checkWinCombinationByTab(String winNumber, String tabNumber, String blinkMe)  {
		
	    String locator = "#reel" + tabNumber + "> div.notch.notch2" ;
	    if (blinkMe.equals("Yes"))locator = "#reel" + tabNumber + "> div.notch.notch2.blinkme";
	
			try {
		   		assertEquals( winNumber, component.driver.findElement(By.cssSelector(locator)).getText());
			
		   	} catch(Error e) {
		   		logger.error("Information regardin win combination incorrectly displayed in " + tabNumber + " tab." );
		   		return false;
		   	}
		    	 
		    	return true;
				  
		    }
  
  public boolean checkWinCombinationIlluminated(String winCombination, String selector) throws InterruptedException {
		
	    String locator = "tr." + selector + ".achievement.achievement.achievement.achievement.achievement > td";
	    try {
	    	if(component.isElementPresent(By.cssSelector(locator)))
	   		assertEquals( winCombination, component.driver.findElement(By.cssSelector(locator)).getText());
	    	else {
				logger.error("Information regardin win combination incorrectly illuminated for combination: " + winCombination );
    			return false;
			}
	   	} catch (Error e) {
	   		logger.error("Information regardin win combination incorrectly illuminated for combination: " + winCombination  );
	   		return false;
	   	}
	    	
	    	return true;
			  
	    }
  
  public boolean checkWinAmount(Integer expectedAmount, Integer startAmount, Integer endAmount) throws InterruptedException {
	  
	  Integer winAmount = endAmount - startAmount + 1;
	  
	  logger.info("Start balance: " + Integer.toString(startAmount));
	  logger.info("End balance: " + Integer.toString(endAmount));
	  
	  if(!winAmount.equals(expectedAmount)) {
     		logger.error("Win incorrectly calculated. Expexted: " + expectedAmount +" Actual:  "+ Integer.toString(winAmount));
     		return false;
	  }
	    	return true;
			  
	  }
  
  public Integer game ( String gameType, Integer winAmount, String winCombimation ,String winCssIndificator, String testData) throws InterruptedException {
	  
	  Integer errorCounter = 0;
	  logger.info("\r\n" +
  			"Test - Combination " + winCombimation);
    	
  	  Integer startBalance = getBalance();
	  	  
	  runGameWithTestData(testData);
	  
	  for (int i=0; i<5; i++) {
   		String number = Character.toString(testData.charAt(i));
   		String line = Integer.toString(++i);
   		if (gameType.equals("win")&& !number.equals("0")) 
   				if(!checkWinCombinationByTab( number, line, "Yes"))errorCounter++; 
   			else
   				if(!checkWinCombinationByTab( number, line, "No"))errorCounter++; 
   	 }
   	  	    	
  	//check if balance correct after game
  	Integer endBalance = getBalance();
  	
  	if(!checkWinAmount( winAmount, startBalance, endBalance))errorCounter++;
  	
  	 if (gameType.equals("win"))	{
		   	//check if win combination  blinked
		   	if(!checkWinInformation(Integer.toString(winAmount)))errorCounter++;
		    //check if win combination illuminated
		 	if(!checkWinCombinationIlluminated(winCombimation , winCssIndificator ))errorCounter++;
	     }     	      	
    
  	 return errorCounter;
  }
  
    
	public static QaAutomationEngineerTestTaskFixture getInstance() {
	        return INSTANCE;
	    }
}
