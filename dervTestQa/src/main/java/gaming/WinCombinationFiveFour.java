package gaming;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationFiveFour {
	
    private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
      
    
    @Test
    public void testWinCombinationFiveFour() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 400, "4 + 4 + 4 + 4 + 4", "win44444", "44444")));
    	
    }
    
   
}

