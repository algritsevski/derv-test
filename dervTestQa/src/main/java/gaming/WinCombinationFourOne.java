package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationFourOne {
		
	
    private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
  
       
    @Test
    public void testWinCombinationFourOne() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 80, "1 + 1 + 1 + 1", "win1111", "11110")));
    	
    	
    }
    
   }

