package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationThreeTwo {
		
	private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
    
    
    @Test
    public void testWinCombinationThreeTwo() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 120, "2 + 2 + 2", "win222", "22200")));

    	
    }
    
}

