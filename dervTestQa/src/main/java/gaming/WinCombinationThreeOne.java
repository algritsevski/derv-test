package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationThreeOne {
		

    private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
    
    
    @Test
    public void testWinCombinationThreeOne() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 60, "1 + 1 + 1", "win111", "11100")));
    	
    }
    
}

