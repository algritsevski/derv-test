package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationFourTwo {
		
	
    private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
  
       
    @Test
    public void testWinCombinationFourTwo() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 160, "2 + 2 + 2 + 2", "win2222", "2222")));

    }
    
   }

