package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationThreeThree {
		

    private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
    
    
    @Test
    public void testWinCombinationThreeThree() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 180, "3 + 3 + 3", "win333", "33300")));
    	
    }
    
}

