package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationFiveTwo {
		

    private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
      
    
    @Test
    public void testWinCombinationFiveTwo() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 200, "2 + 2 + 2 + 2 + 2", "win22222", "22222")));
    	
    }
    
   
}

