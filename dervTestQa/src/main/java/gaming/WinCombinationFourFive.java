package gaming;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationFourFive {
		

    private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
  
       
    @Test
    public void testWinCombinationFourFive() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 400, "5 + 5 + 5 + 5", "win5555", "55550")));
    	
    }
   }

