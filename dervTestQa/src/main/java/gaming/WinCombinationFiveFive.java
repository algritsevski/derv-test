package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationFiveFive {
		
    private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
      
    
    @Test
    public void testWinCombinationFiveFive() throws Exception {
    	    	
    	assertEquals("0", Integer.toString(fixture.game("win", 500, "5 + 5 + 5 + 5 + 5", "win55555", "55555")));
    	
    	
    }
    
   
}

