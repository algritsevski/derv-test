package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationThreeFive {
		

    private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
    
    
    @Test
    public void testWinCombinationThreeFive() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 300, "5 + 5 + 5", "win555", "55500")));
    	
    	
    }
    
}

