package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationFiveOne {
		
	private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
      
    
    @Test
    public void testWinCombinationFiveOne() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 100, "1 + 1 + 1 + 1 + 1", "win11111", "11111")));
    	
    }
    
   
}

