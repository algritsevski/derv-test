package gaming;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationThreeFour {
	
    private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
    
    
    @Test
    public void testWinCombinationThreeFour() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 240, "4 + 4 + 4", "win444", "44400")));
    	
    }
    
}

