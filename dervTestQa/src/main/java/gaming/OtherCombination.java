package gaming;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class OtherCombination {
	
	private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
   
    
    @Test
    public void testOtherCombination() throws Exception {
    	    	    	
	    assertEquals("0", Integer.toString(fixture.game("loss", 0, "0", "0", "44044")));
	    assertEquals("0", Integer.toString(fixture.game("loss", 0, "0", "0", "50555")));
	    assertEquals("0", Integer.toString(fixture.game("loss", 0, "0", "0", "12345")));	
    	
    }
    
   
}

