package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationFourFour {
		
 QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
         
    @Test
    public void testWinCombinationFourFour() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 320, "4 + 4 + 4 + 4", "win4444", "44440")));
    	
    	
    }
    
   }

