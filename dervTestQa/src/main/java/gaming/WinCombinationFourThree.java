package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationFourThree {
		
    private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
  
       
    @Test
    public void testWinCombinationFourThree() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 240, "3 + 3 + 3 + 3", "win3333", "33330")));
    	
    }
    
   }

