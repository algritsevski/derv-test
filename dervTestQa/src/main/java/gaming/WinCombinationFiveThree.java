package gaming;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import dervtest.QaAutomationEngineerTestTaskFixture;

public class WinCombinationFiveThree {
	
	private QaAutomationEngineerTestTaskFixture fixture = QaAutomationEngineerTestTaskFixture.getInstance();
      
    @Test
    public void testWinCombinationFiveFree() throws Exception {
    	
    	assertEquals("0", Integer.toString(fixture.game("win", 300, "3 + 3 + 3 + 3 + 3", "win33333", "33333")));
    	
    }
    
   
}

