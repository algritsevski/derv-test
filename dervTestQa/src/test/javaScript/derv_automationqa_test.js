const assert = require('assert');

const {Browser, By, Key, until} = require('..');
const {ignore, suite} = require('../testing');

suite(function(env) {
  describe('Games', function() {
    let driver;
    const baseUrl = 'file:///C:/workspace/bitbucket/Test_Task.html';
    let winNumber;
    let startBalance;
    let winInfo;
    let winCombination;
    let endBalance;
    
    before(async function() {
        driver = await env.builder().build();
        await driver.get(baseUrl);
    });

    it('win 1 + 1 + 1', async function() {
      
      await $.pause(10, "sleep");
      startBalance = await getBalance();
      await runGameWithTestData(11100);
      winNumber = await driver.findElement(By.css('#reel1> div.notch.notch2.blinkme')).getText();
      expect(winNumber).equal('1');
      winNumber = await driver.findElement(By.css('#reel2> div.notch.notch2.blinkme')).getText();
      expect(winNumber).equal('1');
      winNumber = await driver.findElement(By.css('#reel3> div.notch.notch2.blinkme')).getText();
      expect(winNumber).equal('1');
      winNumber = await driver.findElement(By.css('#reel4> div.notch.notch2')).getText();
      expect(winNumber).equal('0'); 
      winNumber = await driver.findElement(By.css('#reel5> div.notch.notch2')).getText();
      expect(winNumber).equal('0');
      winInfo = await driver.findElement(By.id('winbox')).getText();
      expect(winInfo).equal('Win 60 coins');
      winCombination = await driver.findElement(By.css('tr.win111.achievement.achievement.achievement.achievement.achievement > td')).getText();
      expect(winCombination).equal('1 + 1 + 1');
      endBalance = await getBalance();
      expect (endBalance-startBalance).equal(59);
            
    });
    
    it('win 1 + 1 + 1 + 1', async function() {
    	
    	await $.pause(10, "sleep");
    	startBalance = await getBalance();
    	await runGameWithTestData(11110);
        winNumber = await driver.findElement(By.css('#reel1> div.notch.notch2.blinkme')).getText();
        expect(winNumber).equal('1');
        winNumber = await driver.findElement(By.css('#reel2> div.notch.notch2.blinkme')).getText();
        expect(winNumber).equal('1');
        winNumber = await driver.findElement(By.css('#reel3> div.notch.notch2.blinkme')).getText();
        expect(winNumber).equal('1');
        winNumber = await driver.findElement(By.css('#reel4> div.notch.notch2.blinkme')).getText();
        expect(winNumber).equal('1'); 
        winNumber = await driver.findElement(By.css('#reel5> div.notch.notch2')).getText();
        expect(winNumber).equal('0');
        winInfo = await driver.findElement(By.id('winbox')).getText();
        expect(winInfo).equal('Win 80 coins');
        winCombination = await driver.findElement(By.css('tr.win111.achievement.achievement.achievement.achievement.achievement > td')).getText();
        expect(winCombination).equal('1 + 1 + 1 +1');
        endBalance = await getBalance();
        expect (endBalance-startBalance).equal(79);
        
      });
    
    function runGameWithTestData (testData){
    	
    	   driver.wait(until.elementLocated(By.id('spinButton')),10000);
    	   driver.wait(until.elementLocated(By.css('#reel1> div.notch.notch2')),10000);
    	   driver.findElement(By.id('testdata')).clear();
    	   driver.findElement(By.id('testdata')).sendKeys(testData);
    	   driver.findElement(By.id('spinButton')).click();
	          	
    }
    
    function getBalance (){
    		return driver.findElement(By.id('balance-value')).getAttribute('value');
    }
       

    //after(() => driver && driver.quit());
  });
});
