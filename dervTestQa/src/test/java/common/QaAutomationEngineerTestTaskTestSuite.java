package common;


import gaming.*;
import common.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	OpenApplication.class,
	WinCombinationThreeOne.class,
	WinCombinationFourOne.class,
	WinCombinationFiveOne.class,
	WinCombinationThreeTwo.class,
	WinCombinationFourTwo.class,
	WinCombinationFiveTwo.class,
	WinCombinationThreeThree.class,
	WinCombinationFourThree.class,
	WinCombinationFiveThree.class,
	WinCombinationThreeFour.class,
	WinCombinationFourFour.class,
	WinCombinationFiveFour.class,
	WinCombinationThreeFive.class,
	WinCombinationFourFive.class,
	WinCombinationFiveFive.class,
	LossCombinationFiveFour.class,
	OtherCombination.class,
	CloseApplication.class
})
public class QaAutomationEngineerTestTaskTestSuite {
}
