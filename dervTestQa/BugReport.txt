1. Win combination 2+2+2 does not win

2. With win combination 4+4+4+4 the last digit is not illuminated in slot
	and in paytable section win combination is not displayed in Italic font 
	and incorrect win amount is present ( Expected: Win 320 coins. Actual: Win 240 coins)

3. With win combination 5+5+5+5+5 the last digit is not illuminated in slot
	 and in paytable section win combination is not displayed in Italic font
	  
4. All win combination which is not needed all columns in slot machine for wining dose not win if win combination is not started from the first column 